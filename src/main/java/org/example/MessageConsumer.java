package org.example;

import org.example.bean.Transaction;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

@EnableBinding(Sink.class)
public class MessageConsumer {

    @StreamListener(Sink.INPUT)
    public void messageListener(Transaction trx){
        System.out.println("message receive : ============================>"+trx);
    }
}
